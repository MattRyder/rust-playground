use std::io;

fn main() {
  println!("Fibonacci simulator 2014 - WITH NO-SCOPE DLC!");
  let mut reader = io::stdin();
  let input = reader.read_line().ok().expect("Failed to read! Error DLC extra.");
  let io_iter: Option<uint> = from_str(input.as_slice().trim());

  match io_iter {
    Some(number) => fibonacci(number),
    None         => println!("Are you even trying, dude?")
  }
}

fn fibonacci(iterations: uint) {
  let mut a = 1;
  let mut b = 1;
  let mut sum = 0u;
  let mut iter = 0u;

  println!("You asked for {} iterations:", iterations);

  loop {
    iter += 1;
    sum = a + b;
    a = b;
    b = sum;
    println!("Iteration {}: {}", iter, sum);

    if iter == iterations {
      break;
    }
  }
}