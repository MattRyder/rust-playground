fn main() {
  let mut sum = 0u32;
  let mut count = 0u32;

  loop {
    count += 1;
    if count % 3 == 0 || count % 5 == 0 {
      sum += count
    }

    if count == 999 {
      println!("Answer: {}", sum);
      break;
    }
  }
}
